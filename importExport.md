We can setup `es6 to es5 transpiler` using
1. Traceur
2. Systemjs

then in index.html
```javascript
  <script>
    System.import('./script.js');
  </script>
```

Now lets make following folder structure:
 - index.html
 - script.js
 - external.js


##### File content
* external.js

  ```javascript
    let keyValue = 2000;

    function test() {
      console.log('hi');
    }

    let ab = 1000;
    export default ab;
    export { keyValue, test };
  ```

* script.js

  ```javascript
    import { keyValue as key, test } from 'external.js';
    import a from 'external.js';
  ```
> Note: if something is exported as default then we can import it using any name but
> if it's not default export then we need to import in object literal the same name.

* import { keyValue as key, test } from 'external.js';
  import a from 'external.js'; can be written as
  import a, { keyValue as key, test } from 'external.js';

* Also we can write
  import * as CUSTOM_NAME from 'external.js';

  then we can access all the props

##### Todo:
  1. make a repo for this whole setup
  2. check if default can be imported using `*`.


>There are two important Rules, which you need to understand if you're working with ES6 Modules:
>	  1. Modules are always in Strict Mode (no need to define "use strict")
>	  2. Modules don't have a shared, global Scope. Instead each Module has its own Scope
