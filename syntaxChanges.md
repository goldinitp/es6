#### What?
This note cover some of the tricky edge cases in new ES6 syntax

##### Destructuring Array
```javascript
let numbers = [1, 2, 3];

let [a, , c] = numbers;

console.log(a,c);
```
output
> 1, 3

##### Destructuring Object

1. above case doesn't hold true with objects since its referenced by name and not position.

	So,
	```javascript
	let Person = {
		name: 'Joey',
		age: '24',
		greet: function() {
			console.log(Hi);
		}
	}

	let {a, , c} = Person;
	```

	It throws an error `Uncaught SyntaxError: Unexpected token ,`

2. Alias in Object:

	```javascript
	let Person = {
		name: 'Joey',
		age: '24',
		greet: function() {
			console.log(Hi);
		}
	}
	
	let { name: nameAlias, age, greet} = Person;
	console.log(nameAlias);
	```
	output
	> Joey
	
	but if we do
	```javascript
	console.log(name)
	```
	it won't console anything.

##### Rest and Spread Operator
 1. rest Operator: 
 	```javascript
	/* 
	   Used when we dont have specified no of arguments that a
	   function can accept. Arguments are always dynamic.
	   make arguments an array which is stored in `args` here.
	*/
	function RestOperator(...args) {
		console.log(args)
		// makes args an array
	}
	```
 2. Spread Operator:
 ```javascript
 	/*
		when `...` is passed in function call.
	*/
	
	let numbers = [1,2,3,4,5];
	
	console.log(Math.max(...numbers)) // ... passed in function call
	```
	

